% num=xlsread('20x4 Motor operation TF96.180.xlsx');
% torque=-1.*num(:,[3,4]);
% RPM=num(:,[1,2]);
% w=RPM.*0.104719755;
dt=0.1;
maxaccum=0;
legendon=1;
 figure
    for i = 11:22
        load(['LNMPC_output_Kpto300_SF1e+08_Th08_eta100_lam1.000e-12_lpr1.000e-12_ss',num2str(i,'%02.f'),'_Fb2465522_BvdMulti7_17_21_PTOFlim1028795_PTOPlim1923072.mat'])
        instpower=sum(InstPowerElec_sim.data(:,1:3),2);
        accum=zeros(length(instpower),1);
        for j = 2:length(instpower)
            accum(j)=max(0,accum(j-1)+dt.*instpower(j));
        end

%          accumdat(:,i)=accum';
         subplot(3,4,i-10)
         plot(dt.*[1:length(accum)]',1e-6.*instpower)
         xlabel('Time (s)')
         ylabel(['Energy Defecit (MJ)',newline,'Instantaneous Power (MW)'])
         title(['SS ',num2str(i),newline,'Max Accum = ',num2str(max(1e-6.*accum),'%.2f'), 'MJ',newline,'Max Motoring Power = ',num2str(max(1e-6.*instpower),'%.2f'), 'MW',newline,'Max Generating Power = ',num2str(min(1e-6.*instpower),'%.2f'), 'MW']) 
         hold on 
         plot(dt.*[1:length(accum)],1e-6.*accum)
         if legendon==1
            legend('Electrical Power','Energy Deficit','location','bestoutside')
            legendon=legendon-1;
         end
         clear accum
         clear instpower
    end
sgtitle('Energy Accumulation in a 3 PTO powersharing CETO device')